#include <iostream>
#include <fstream> // For file I/O
#include <string>
using namespace std;

int main() {
    // Basic console input and output
    cout << "Enter your name: ";
    
    string name;
    cin >> name; // Reads a single word (stops at space or newline)
    
    cout << "Hello, " << name << "!" << endl;

    // Basic file input and output
    ofstream outputFile("output.txt"); // Creating an output file stream
    if (outputFile.is_open()) {
        outputFile << "This is a line written to the file." << endl;
        outputFile << "Hello, " << name << "!" << endl;
        outputFile.close();
        cout << "Data written to the file 'output.txt'" << endl;
    } else {
        cerr << "Unable to open the file for writing." << endl;
    }

    ifstream inputFile("input.txt"); // Creating an input file stream
    if (inputFile.is_open()) {
        string line;
        cout << "Contents of the file 'input.txt':" << endl;
        while (getline(inputFile, line)) {
            cout << line << endl;
        }
        inputFile.close();
    } else {
        cerr << "Unable to open the file for reading." << endl;
    }

    return 0;
}
