#include<iostream>
#include<string>
class Car{
private:
int speed;
int model;

public:
void setModel(int modelValue){
    model = modelValue;
}
void speedUp(){
    speed += 10;
}
void slowDown(){
   if(speed <= 10){
    speed = 0;
   }else {
    speed -=10;
   }
}
void stop(){
    speed = 0;
}
void display(){
    std::cout<<"Model is "<<model<<" and running at speed of "<<speed<<std::endl;
}

};

int main(){
    Car reemCar;
    reemCar.setModel(2024);
    reemCar.speedUp();
    reemCar.speedUp();
    reemCar.setModel(2020);
    Car ahmedCar;
    ahmedCar.speedUp();
    ahmedCar.speedUp();
    ahmedCar.speedUp();
    ahmedCar.setModel(2023);
    
    reemCar.slowDown();
    reemCar.slowDown();
    reemCar.slowDown();


    // std::cout<<"Reem's Car model is "<<reemCar.model<< " and speed = " <<reemCar.speed<<std::endl;
    // std::cout<<"Ahmed's car model is "<<ahmedCar.model<<" and speed = "<<ahmedCar.speed<<std::endl;

    reemCar.display();
    ahmedCar.display();

    std::cout<<"C++ is fun and easy \n";
    std::cout<<"Astaghfr Allah"<<std::endl;
    return 0;
}
