#include <iostream>
using namespace std;

// Forward declaration of the Car class
class Car;

// Friend function declaration
void displayMileage(const Car& car);

// Car class
class Car {
private:
    string brand;
    double mileage;  // Private member

public:
    Car(string b, double m) : brand(b), mileage(m) {}

    // Friend function declaration
    friend void displayMileage(const Car& car);
};

// Friend function definition
void displayMileage(const Car& car) {
    cout << "Mileage of the car (brand: " << car.brand << ") is: " << car.mileage << " miles." << endl;
}

int main() {
    // Creating an instance of the Car class
    Car myCar("Toyota", 25.5);

    // Calling the friend function to display the private member
    displayMileage(myCar);

    return 0;
}
