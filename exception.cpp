#include <iostream>
using namespace std;

// Function that throws an exception
double divideNumbers(int numerator, int denominator) {
    if (denominator == 0) {
        throw "Division by zero is not allowed";
    }
    return static_cast<double>(numerator) / denominator;
}

int main() {
    int numerator, denominator;

    cout << "Enter numerator: ";
    cin >> numerator;

    cout << "Enter denominator: ";
    cin >> denominator;

    try {
        // Attempting the division operation that may throw an exception
        double result = divideNumbers(numerator, denominator);
        cout << "Result of division: " << result << endl;
    } catch (const char* errorMessage) {
        // Catching the exception and handling it
        cerr << "Error: " << errorMessage << endl;
    } catch (const exception& e) {
        // Catching other exceptions (not used in this example)
        cerr << "Exception: " << e.what() << endl;
    }

    return 0;
}
