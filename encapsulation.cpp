#include <iostream>
using namespace std;

class BankAccount {
private:
    string accountNumber;
    double balance;

public:
    void setAccountDetails(string accNum, double initialBalance) {
        accountNumber = accNum;
        balance = initialBalance;
    }

    void displayBalance() {
        cout << "Account Number: " << accountNumber << ", Balance: $" << balance << endl;
    }
};

int main() {
    BankAccount myAccount;

    // Accessing private members through public methods
    myAccount.setAccountDetails("123456789", 1000.0);
    myAccount.displayBalance();

    return 0;
}
