#include <iostream>
using namespace std;

// Abstract base class
class Shape {
public:
    virtual void draw() = 0;  // Pure virtual function (making the class abstract)
};

// Concrete derived class implementing draw
class Circle : public Shape {
public:
    void draw() override {
        cout << "Drawing a circle." << endl;
    }
};

int main() {
    // Cannot create an instance of an abstract class
    // Shape abstractShape;  // This line would result in an error

    Circle circleShape;
    circleShape.draw();

    return 0;
}
