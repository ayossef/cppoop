#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

// Class to represent a Book
class Book {
public:
    string title;
    string author;
    string isbn;
    string genre;
    bool available;

    Book(const string& t, const string& a, const string& i, const string& g)
        : title(t), author(a), isbn(i), genre(g), available(true) {}

    // Display book details
    void displayInfo() const {
        cout << "Title: " << title << "\nAuthor: " << author << "\nISBN: " << isbn << "\nGenre: " << genre
             << "\nAvailability: " << (available ? "Available" : "Not Available") << endl;
    }
};

// Class to represent a User
class User {
public:
    string name;
    string contact;
    string libraryID;

    User(const string& n, const string& c, const string& id)
        : name(n), contact(c), libraryID(id) {}

    // Display user details
    void displayInfo() const {
        cout << "Name: " << name << "\nContact: " << contact << "\nLibrary ID: " << libraryID << endl;
    }
};

// Class to represent the Library and manage books and users
class Library {
private:
    vector<Book> books;
    vector<User> users;

public:
    // Add a new book to the library
    void addBook(const Book& book) {
        books.push_back(book);
    }

    // Add a new user to the library
    void addUser(const User& user) {
        users.push_back(user);
    }

    // Display a list of available books
    void displayAvailableBooks() const {
        cout << "Available Books:\n";
        for (const Book& book : books) {
            if (book.available) {
                book.displayInfo();
                cout << "---------------------\n";
            }
        }
    }

    // Search for a book by title
    Book* searchBookByTitle(const string& title) {
        for (Book& book : books) {
            if (book.title == title) {
                return &book;
            }
        }
        return nullptr; // Book not found
    }

    // Display user details by library ID
    User* findUserByLibraryID(const string& libraryID) {
        for (User& user : users) {
            if (user.libraryID == libraryID) {
                return &user;
            }
        }
        return nullptr; // User not found
    }

    // Borrow a book
    void borrowBook(const string& title, const string& libraryID) {
        Book* book = searchBookByTitle(title);
        User* user = findUserByLibraryID(libraryID);

        if (book && user && book->available) {
            book->available = false;
            cout << user->name << " has successfully borrowed " << book->title << "." << endl;
        } else {
            cout << "Book not available or user not found." << endl;
        }
    }

    // Return a book
    void returnBook(const string& title) {
        Book* book = searchBookByTitle(title);

        if (book && !book->available) {
            book->available = true;
            cout << book->title << " has been successfully returned." << endl;
        } else {
            cout << "Book not found or already returned." << endl;
        }
    }
};

int main() {
    // Sample Book and User instances
    Book book1("The Catcher in the Rye", "J.D. Salinger", "978-0-316-76948-0", "Fiction");
    Book book2("To Kill a Mockingbird", "Harper Lee", "978-0-06-112008-4", "Classics");

    User user1("John Doe", "john.doe@email.com", "LIB001");
    User user2("Jane Smith", "jane.smith@email.com", "LIB002");

    // Sample Library instance
    Library library;

    // Add books and users to the library
    library.addBook(book1);
    library.addBook(book2);

    library.addUser(user1);
    library.addUser(user2);

    // Display available books
    library.displayAvailableBooks();

    // Simulate borrowing and returning books
    library.borrowBook("To Kill a Mockingbird", "LIB001");
    library.returnBook("To Kill a Mockingbird");

    return 0;
}
