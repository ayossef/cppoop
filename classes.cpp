#include <iostream>
using namespace std;

class Car {
public:
    string brand;
    string model;
    int year;

    void displayInfo() {
        cout << "Brand: " << brand << ", Model: " << model << ", Year: " << year << endl;
    }
};

int main() {
    // Creating objects of the Car class
    Car car1, car2;

    // Initializing object properties
    car1.brand = "Toyota";
    car1.model = "Camry";
    car1.year = 2020;

    car2.brand = "Honda";
    car2.model = "Civic";
    car2.year = 2019;

    // Calling a method to display information
    car1.displayInfo();
    car2.displayInfo();

    return 0;
}
